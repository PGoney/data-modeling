function ret = GE(A, b)
    [row, col] = size(A);
    for i = 1:row
        pivot = A(i, i);
        for j = i+1:row
            mul = A(j,i)/pivot;
            A(j,:) = A(j,:) - mul*A(i,:);
            b(j) = b(j) - mul*b(i);
        end
    end
    for i = row:-1:1
        pivot = A(i, i);
        for j = i-1:-1:1
            mul = A(j,i)/pivot;
            A(j,:) = A(j,:) - mul*A(i,:);
            b(j) = b(j) - mul*b(i);
        end
        b(i) = b(i) / pivot;
    end
    ret = b;
end