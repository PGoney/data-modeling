function p = polyEquivInterval(func, a, b, partition_size, increment)
    p = zeros(1, ceil((b - a)/increment + 1));
    
    x = linspace(a, b, partition_size+1);
    y = double(subs(func, x)');
    c = coef(partition_size+1, x, y);
    
    for i = 1:length(p)
        p(i) = evalNewtonsForm(partition_size+1, x, c, a + (i-1)*increment);
    end
end