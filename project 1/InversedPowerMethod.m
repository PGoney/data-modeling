function [eig_value, eig_vector] = InversedPowerMethod(A, q, x, N, TOL)
    [row, col] = size(A);
    A = A - q*eye(row); 
    mu = NaN;
    for i = 1:N
        w = GE(A, x);
        old_mu = mu;
        mu = w(1)/x(1);
        x = w/norm(w,inf);
        if abs(mu - old_mu) < TOL
            break;
        end
    end
    eig_value = q + 1/mu;
    eig_vector = x/norm(x);
end