function [history, ret] = RungeKuttaMethod(ode_func, x_span, y0)
    history = [x_span(1), y0];
    ret = y0;
    
    y_cur = y0;
    for i = 1:length(x_span)-1
        h = x_span(i+1) - x_span(i);
        
        k1 = h * ode_func(x_span(i), y_cur);
        k2 = h * ode_func(x_span(i) + h/2, y_cur + k1/2);
        k3 = h * ode_func(x_span(i) + h/2, y_cur + k2/2);
        k4 = h * ode_func(x_span(i) + h, y_cur + k3);
        
        y_next = y_cur + 1/6 * (k1 + 2*k2 + 2*k3 + k4);
        
        history = [history; x_span(i+1), y_next];
        
        y_cur = y_next;
    end
    
    ret = y_next;
end