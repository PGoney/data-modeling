function [x, iter] = SOR(A, b, x, w, MAX_ITER, TOL)
    D = diag(diag(A));
    L = tril(A, -1);
    U = triu(A, 1);
    
    M = D + w*L;
    N = (1-w)*D - w*U;
    b = w*b;
    
    for i=1:MAX_ITER
        x_old = x;
        x = M\(N*x + b);
        
        if norm(x - x_old, 2) < TOL
            iter = i;
            return
        end
    end
    
%     x = NaN;
    iter = NaN;
end