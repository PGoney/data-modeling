function [history, ret] = EulerMethod2(ode_func1, ode_func2, x_span, y0)
    history = [x_span(1), y0];
    ret = y0;
    
    y_cur = y0;
    for i = 1:length(x_span)-1
        h = x_span(i+1) - x_span(i);
        y_next = y_cur + h*ode_func1(x_span(i), y_cur) + h^2/2*ode_func2(ode_func1, x_span(i), y_cur);
        
        history = [history; x_span(i+1), y_next];
        
        y_cur = y_next;
    end
    
    ret = y_next;
end