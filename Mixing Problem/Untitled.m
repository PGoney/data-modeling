clear; clc;

t_span = 0:0.1:300;
x0 = 0;

[t, x] = ode45(@myode, t_span, x0);

[history_1, ret_1] = EulerMethod(@myode, t_span, 0);
[history_2, ret_2] = ImprovedEulerMethod(@myode, t_span, 0);
[history_3, ret_3] = RungeKuttaMethod(@myode, t_span, 0);
[history_4, ret_4] = EulerMethod2(@myode, @myode2, t_span, 0);

err_1 = x - history_1(:, 2);
err_2 = x - history_2(:, 2);
err_3 = x - history_3(:, 2);
err_4 = x - history_4(:, 2);

history = [t, x, history_1(:, 2), history_2(:, 2), history_3(:, 2), err_1, err_2, err_3];

function dx = myode(t, x)
    dx = 6 - 5*x(1)/(1000 + t);
end

function dx = myode2(ode_func, t, x)
    dx = - (5*ode_func(t, x)*(1000 + t) - 5*x)/(1000 + t)^2;
end