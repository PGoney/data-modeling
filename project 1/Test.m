A = [-15/500, 5/1000, 0, 0, 0;
    15/500, -17/1000, 2/400, 0, 0;
    0, 12/1000, -17/400, 5/700, 0;
    0, 0, 15/400, -20/700, 5/500;
    0, 0, 0, 15/700, -15/500];

V = [500; 1000; 400; 700; 500];
f = [50; 0; 0; 0; 0];
X0 = [10; 0; 0; 0; 0];

circle = GersgorinCircle(A);
fprintf("Gersgorin Circle\n")
disp(circle)
q = [0, -0.01, -0.05, -0.034, -0.038]';

% A = [-15/500, 5/1500, 0, 0, 0;
%     15/500, -18/1500, 3/500, 0, 0;
%     0, 13/1500, -15/500, 2/1000, 0;
%     0, 0, 12/500, -19/1000, 7/600;
%     0, 0, 0, 17/1000, -17/600];
% V = [500; 1500; 500; 1000; 600];
% f = [70; 0; 0; 0; 0];
% X0 = [10; 0; 0; 0; 0];
% q = [-0.004, -0.008, -0.02, -0.034, -0.040]';

eig_value = zeros(5,1);
eig_vector = zeros(5,5);
for i = 1:5
    [eig_value(i), eig_vector(:,i)] = InversedPowerMethod(A, q(i), [1,1,1,1,1]', 1000, 10^-8);
end

fprintf("Eigenvalue\n")
disp(eig_value .* eye(5))

fprintf("Eigenvector\n")
disp(eig_vector)

% temp = -eig_vector(:,4);
% eig_vector(:,4) = -eig_vector(:,5);
% eig_vector(:,5) = temp;

Xp = -GE(A,f);
c = GE(eig_vector, X0 - Xp);

X = [];

for i = 0:5:20
    X = [X, eig_vector*(c.*exp(eig_value*i)) + Xp];
end

X = X ./ V;

fprintf("Concentration\n")
for i = 1:5
    fprintf("%d\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\n", 5*(i-1), X(:,i))
end