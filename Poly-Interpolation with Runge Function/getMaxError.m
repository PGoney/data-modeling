function [index, max_error] = getMaxError(error)
    index = 1;
    max_error = error(1);
    for i = 2:length(error)
        cur_error = error(i);
        if max_error < cur_error
            index = i;
            max_error = cur_error;
        end
    end
end