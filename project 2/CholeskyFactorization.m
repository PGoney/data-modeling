function ret = CholeskyFactorization(A)
    [row, col] = size(A);
    for k = 1:row
        A(k, k) = sqrt(A(k, k));
        for i = k+1:row
            A(i, k) = A(i, k) / A(k, k);
        end
        for j = k+1:row
            for i = j:row
                A(i, j) = A(i, j) - A(i, k)*A(j, k);
            end
        end
    end
    ret = tril(A);
end