clear;

nx_list = [10, 20, 30];
% n_list = [5];
info_output = ["n", "SOR_w", "SOR", "CG", "PCG1", "PCG2", "PCG3", "PCG4", "PCG5"];
output_list = [];

MAX_ITER = 1e4;
TOL = 1e-4;

for i = 1:length(nx_list)
    nx = nx_list(i);
    n = 3*nx^2 + 4*nx + 1;

    A = gallery('wathen', nx, nx);
    b = A*ones(n, 1);
    x = zeros(n, 1);
    
    w_list = zeros(199, 1);
    iter_list = zeros(199, 1);
    index = 1;
    
    min_w = NaN;
    min_iter = inf;
    
    for w = 0.01:0.01:1.99
        [x_opt, iter] = SOR(A, b, x, w, MAX_ITER, TOL);
        
        w_list(index) = w;
        iter_list(index) = iter;
        index = index + 1;
        
        if min_iter > iter
            min_iter = iter;
            min_w = w;
        end
    end
    
    SOR_w = min_w;
    SOR_iter = min_iter;
    
    [x_opt, CG_iter] = CG(A, b, x, MAX_ITER, TOL);

    I = eye(n);
    D = diag(diag(A));
    L = tril(A, -1);
%     M = IncompleteCholeskyFactorization(A, 'poisson');
    H = ichol(A);
    
    [x_opt, PCG_1_iter] = PCG(A, b, x, I, MAX_ITER, TOL);
    [x_opt, PCG_2_iter] = PCG(A, b, x, D, MAX_ITER, TOL);
    [x_opt, PCG_3_iter] = PCG(A, b, x, (D+L)*(D+L)', MAX_ITER, TOL);
    [x_opt, PCG_4_iter] = PCG(A, b, x, (D+L)*inv(D)*(D+L)', MAX_ITER, TOL);
    [x_opt, PCG_5_iter] = PCG(A, b, x, H*H', MAX_ITER, TOL);
    
    output_list = [output_list; [n, SOR_w, SOR_iter, CG_iter, PCG_1_iter ...
        PCG_2_iter, PCG_3_iter, PCG_4_iter, PCG_5_iter]];
end

fp = fopen('result2.txt', 'w');
% fprintf("%4s\t%4s\t%4s\t%4s\t%4s\t%4s\t%4s\t%4s\t%4s\n", info_output');
fprintf(fp, "%4d\t%4.2f\t%4d\t%4d\t%4d\t%4d\t%4d\t%4d\t%4d\n", output_list');
fclose(fp);