function [x, iter] = CG(A, b, x, MAX_ITER, TOL)
    r = b - A*x;
    p = r;
    for i=1:MAX_ITER
        if norm(r, 2) < TOL
            iter = i;
            return
        end
        
        q = A*p;
        alpha = (r'*r)/(p'*q);
        x = x + alpha*p;
        r_old = r;
        r = r - alpha*q;
        beta = (r'*r)/(r_old'*r_old);
        p = r + beta*p;
    end

%     x = NaN;
    iter = NaN;
end