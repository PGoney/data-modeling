function [x, iter] = PCG(A, b, x, M, MAX_ITER, TOL)
    r = b - A*x;
    p = M\r;
    z = p;
    for i=1:MAX_ITER
        if norm(r, 2) < TOL
            iter = i;
            return
        end
        
        q = A*p;
        alpha = (r'*z)/(p'*q);
        x = x + alpha*p;
        r_old = r;
        r = r - alpha*q;
        z_old = z;
        z = M\r;
        beta = (r'*z)/(r_old'*z_old);
        p = z + beta*p;
    end
    
%     x = NaN;
    iter = NaN;
end