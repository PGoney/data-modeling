function ret = getError(y, p)
    n = length(y);
    error = zeros(1, n);
    for i = 1:n
        error(i) = abs(double(y(i) - p(i)));
    end
    ret = error;
end