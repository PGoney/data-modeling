function ret = GersgorinCircle(A)
    [row, col] = size(A);
    circle = zeros(row, 2);
    
    for i = 1:row
        sum = 0;
        for j = 1:col
            if j == i
                continue
            end
            sum = sum + abs(A(i,j));
        end
        circle(i,1) = A(i,i) - sum;
        circle(i,2) = A(i,i) + sum;
    end
    
    ret = circle;
end