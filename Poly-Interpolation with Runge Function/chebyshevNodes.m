function ret = chebyshevNodes(a, b, n)
    x = zeros(1, n+1);
    for i = 1:n+1
        x(i) = (a+b)/2 - (b-a)*cos((pi*(2*i-1)/(2*n+2)))/2;
    end
    ret = x;
end