function ret = IncompleteCholeskyFactorization(A, structure)
    [row, col] = size(A);
    
    if structure == "poisson"
        for k = 1:row
            A(k, k) = sqrt(A(k, k));
            [row_list, col_list, value_list] = find(A(k+1:row, k));
            for i = 1:length(row_list)
                row_index = k + row_list(i);
                A(row_index, k) = A(row_index, k) / A(k, k);
            end
            [row_list, col_list, value_list] = find(A(k+1:row, k+1:row));
            for i = 1:length(row_list)
                row_index = k + row_list(i);
                col_index = k + col_list(i);
                A(row_index, col_index) = A(row_index, col_index) - A(row_index, k)*A(col_index, k);
            end
        end
        ret = tril(A);
        return
    end
    
    for k = 1:row
        A(k, k) = sqrt(A(k, k));
        for i = k+1:row
            if A(i, k) ~= 0
                A(i, k) = A(i, k) / A(k, k);
            end
        end
        for j = k+1:row
            for i = j:row
                if A(i, j) ~= 0
                    A(i, j) = A(i, j) - A(i, k)*A(j, k);
                end
            end
        end
    end
    ret = tril(A);
end