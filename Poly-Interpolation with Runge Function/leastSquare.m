function ret = leastSquare(A, b)
    x = inv(A'*A)*A'*b;
    ret = x;