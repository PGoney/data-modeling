function ret = evalNewtonsForm(n, x, a, t)
    temp = a(n);
    for i = n-1:-1:1
        temp = temp*(t - x(i)) + a(i);
    end
    ret = temp;
end