function [eig_value, eig_vector] =PowerMethod(A, x, N, TOL)
    mu = NaN;
    for i = 1:N
        w = A*x;
        old_mu = mu;
        mu = w(1)/x(1);
        x = w/norm(w,inf);
        if abs(mu - old_mu) < TOL
            disp(i)
            break
        end
    end
    eig_value = mu;
    eig_vector = x/norm(x);
end