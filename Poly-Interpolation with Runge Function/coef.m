function ret = coef(n, x, y)
    a = y';
    for j = 2:n
        for i = n:-1:j
            a(i) = (a(i) - a(i-1))/(x(i) - x(i-j+1));
        end
    end
    ret = a;
end